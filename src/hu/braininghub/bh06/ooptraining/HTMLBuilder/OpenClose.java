package hu.braininghub.bh06.ooptraining.HTMLBuilder;

public interface OpenClose {
  String open();
  String close();

}
