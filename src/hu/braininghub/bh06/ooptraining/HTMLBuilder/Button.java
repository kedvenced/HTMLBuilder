package hu.braininghub.bh06.ooptraining.HTMLBuilder;

public class Button implements OpenClose{
    
  String title;
  String open;
  String close;
  
  public Button(String title) {
    this.open = "<button type=\"button\" >";
    this.close = "</button>";
    this.title = title;
  }

  @Override
  public String open() {
    return open;
  }

  @Override
  public String close() {
    return close;
  }

  @Override
  public String toString() {
    return open + title + close;
  }

  
  
}