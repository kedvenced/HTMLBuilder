package hu.braininghub.bh06.ooptraining.HTMLBuilder;

public class Text implements OpenClose{
  
  private String open;
  private String close;
  private String text;
  
  public Text(int row, int cols, String text) {
    this.open="<textarea rows="+row+" cols="+cols+">";
    this.close="</textarea>";
    this.text=text;
  }
  
  
  @Override
  public String open() {
    return open;
  }

  @Override
  public String close() {
    return close;
  }

  @Override
  public String toString() {
    return open+"\n  "+text+"\n"+close;
  }
    
}
