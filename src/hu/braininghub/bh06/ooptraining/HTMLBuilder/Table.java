/*
 */
package hu.braininghub.bh06.ooptraining.HTMLBuilder;

/**
 *
 * @author Petya
 */
public class Table implements OpenClose{
  
  private String open;
  private String close;
  private String bodyCode;
  private int row;
  private int height;
  
  public Table(int row, int height) {
    this.open = "<table>";
    this.close = "</table>";
    this.row = row;
    this.height = height;
    if (row>=1 && height>=1) {
      tableBody(row, height);
    }
  }
  
  String tableBody(int row, int height){
    String rowOpen = "<tr>";
    String rowClose = "</tr>";
    
    //rowmaker
    for (int i = 0; i < row; i++) {     
      String cycleCode;
      cycleCode = "<th>"+i+"</th>\n";
      bodyCode += cycleCode;
    }
    bodyCode = rowOpen+bodyCode+rowClose;
    
    //heightmaker
    for (int i = 0; i < height; i++) {
      String cycleCode;
      cycleCode = "<th>"+i+"</th>\n";
      bodyCode += cycleCode;
    }
    
  return bodyCode;
  }
  
  
  @Override
  public String open() {
    return open;
  }

  @Override
  public String close() {
    return close;
  }
  
  
}
