/* A program másodperc alapon kalkulál. 
Egy átlagos nappali órában gyors dalok szólnak, 2:30 és 3:30 hosszúak,
míg egy éjszakai órában a lassú zenék dominálnak, 3:30 és 4:30 között. 
*/
public class NextGen {
  
  final int keszOra=3600, hirek=180; 
  static final int E=200;
  static int [] dalok=new int [E];
  static int [] voltmar=new int [E];
  boolean ok;
  
  static void tombFeltoltes(){
          for (int i = 0; i < E; i++) {
            dalok[i]=((int)(Math.random()*120)+150);
          }
  }

  static void voltmar(){
    int db=0;
    for (int i = 0; i < E; i++) {
      int perc=voltmar[i]/60;
      int mp=voltmar[i]%=60;
      if (voltmar[i]!=0){
      System.out.print(""+perc);
        if (mp < 10)
          System.out.print(":0"+mp+", ");
        else 
          System.out.print(":"+mp+", ");
        System.out.println();
        db++;
      }
    }
    System.out.println("\n"+db+" db");
    menu();
  }
    
  static void menu(){
    int valasz;
    boolean ok;
    System.out.println("Menü");
    do {
      System.out.println("\nNappali óra generálása (1)"+
            "\nÉjszakai óra generálása (2)"+
            "\nDalok, amik már voltak (3)"+
            "\nKilépés (0)");
      valasz=extra.Console.readInt("Melyik legyen? ");
      ok=(-1<valasz && valasz<4);
      if (!ok)
        System.out.println("Ez nem valódi menüpont!");
      switch (valasz){
        case 1: orakepzesNappal(); break;
        case 2: orakepzesEjjel(); break;
        case 3: voltmar(); break;
        case 0: break;
      }
    }while (!ok);
  }
  
  static void orakepzesNappal(){
    int egesz=0, i=0, dalDb=0, mellement=0;
    System.out.println("\nGenerált nappali óra:\n"
            + "\nHírek (3:00)");
    while(egesz<3420) {
      i=((int)(Math.random()*E));
      if (dalok[i]<210 && 90<=dalok[i] && voltmar[i]!=dalok[i]){
        egesz+=dalok[i];
        int perc=dalok[i]/60;
        int mp=dalok[i]%=60;
        voltmar[i]=dalok[i];
        System.out.print(perc+":");
        if (mp < 10)
          System.out.print("0"+mp+", ");
        else 
          System.out.print(mp+", ");
        System.out.println();
        dalDb++;
      }
      else mellement++;
    }
    System.out.print("(Összesen "+dalDb+" dalból összeállítva "
            +egesz/60+":");
        if ((egesz%=60) < 10)
          System.out.println("0"+(egesz%=60)+" hosszban)");
        else 
          System.out.println((egesz%=60)+" hosszban)");
        System.out.println("Mellément "+mellement+" db.\n");
    menu();
  }    
  
  static void orakepzesEjjel(){
    int egesz=0, i=0, mellement=0, dalDb=0;
    System.out.println("\nGenerált éjszakai óra");
    while(egesz<3600) {
      i=((int)(Math.random()*E));
      if (dalok[i] >= 180){
        egesz+=dalok[i];
        int perc=dalok[i]/60;
        int mp=dalok[i]%=60;
        voltmar[i]=dalok[i];
        System.out.print(perc+":");
        if (mp < 10)
          System.out.print("0"+mp+", ");
        else 
          System.out.print(mp+", ");
        System.out.println();
        dalDb++;
      }
      else mellement++;
    }
    System.out.print("(Összesen "+dalDb+" dalból összeállítva "
            +egesz/60+":");
        if ((egesz%=60) < 10)
          System.out.println("0"+(egesz%=60)+" hosszban)");
        else 
          System.out.println((egesz%=60)+" hosszban)");
        System.out.println("Mellément "+mellement+" db.\n");
    menu();
  }
  
  public static void main(String[] args) {
    System.out.println("NextGen Station - rádiós műsorszerkesztő"+"\n");
    int dalLista, menu, dalDb, hibásDal, dal;
    boolean ok;
    tombFeltoltes();    
    menu();
    System.out.println("\nKöszönjük, hogy a NextGen Stationt használja!"); 
  }  
}