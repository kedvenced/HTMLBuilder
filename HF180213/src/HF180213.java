/* A program másodperc alapon kalkulál. 
Egy átlagos nappali órában gyors dalok szólnak, 2:30 és 3:30 hosszúak,
míg egy éjszakai órában a lassú zenék dominálnak, 3:30 és 4:30 között. 
*/
public class HF180213 {
  public static void main(String[] args) {
    System.out.println("NextGen Station - rádiós műsorszerkesztő"+"\n");
    final int készÓra=3600, hírek=180; 
    int dalLista, menu, dalDb, hibásDal;
    boolean ok;
    System.out.println("Menü");
    do {
      System.out.println("\nNappali óra generálása (1)"+
            "\nÉjszakai óra generálása (2)"+
            "\nKilépés (0)");
      menu=extra.Console.readInt("Melyik legyen? ");
      ok=(-1<menu && menu<3);
      if (!ok)
        System.out.println("Ez nem valódi menüpont!");
      if (ok)  
      switch (menu){
        case 1: {
          int dal;
          int felesHírek=extra.Console.readInt("Vannak-e feles hírek?"+
                  "\nIgen (1) \nNem (2)\nVálasz: ");
          dalLista=0; dalDb=0; hibásDal=0;
          if (felesHírek==1) {
            System.out.println("\nNappali óra dalai "+
                          "\n"+"\nHírek (3:00)");
            int feles1=0;
            while (feles1<=((készÓra/2)-hírek)) {
            dal=((int)(Math.random()*210)+1);
              if(dal>150) {
                feles1+=dal;
                dalDb++;
                System.out.println((dal/60)+":"+(dal%=60)+", ");
              }
              else hibásDal++;
            }
            System.out.println("Feles hírek (3:00)");
            while (dalLista<=((készÓra/2)-hírek)) {
            dal=((int)(Math.random()*210)+1);
              if(dal>150) {
                dalLista+=dal;
                dalDb++;
                System.out.println((dal/60)+":"+(dal%=60)+", ");
              }
              else hibásDal++;
            } 
            System.out.println("(Összesen "+dalDb+" dalból összeállítva"+
                              "\nértéken kívül generált dal: "+hibásDal+" db)");
            break;
          }
          else 
            System.out.println("\nNappali óra dalai "+
                          "\n"+"\nHírek (3:00)");
            dalLista=0; dalDb=0; hibásDal=0;
            while (dalLista<=(készÓra-hírek)) {
            dal=((int)(Math.random()*210)+1);
              if(dal>150) {
                dalLista+=dal;
                dalDb++;
                System.out.println((dal/60)+":"+(dal%=60)+", ");
              }
              else hibásDal++;
            }
            System.out.println("(Összesen "+dalDb+" dalból összeállítva"+
                               "\nértéken kívül generált dal "+hibásDal+" db)");
          break;
        }
        case 2: {
          System.out.println("\nÉjszakai óra dalai "+
                  "\n");
          dalLista=0; dalDb=0; hibásDal=0;
          int dal;
          while (dalLista<=készÓra) {
          dal=((int)(Math.random()*270)+1);
            if(dal>210) {
              dalLista+=dal;
              dalDb++;
              System.out.println((dal/60)+":"+(dal%=60)+", ");
            }
            else hibásDal++;
          }
          System.out.println("(Összesen "+dalDb+" dalból összeállítva"+
                        "\nértéken kívül generált dal "+hibásDal+" db)");
          System.out.println("");
          break;
        }
        case 0:
        System.out.println("\nKöszönjük, hogy a NextGen Stationt használja!"); 
        break;
      } //switch
    }while(menu!=0);
}
}
